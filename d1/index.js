// INTRO TO JSON


// JSON Object
/*
{
    "city": "Qiezon City",
    "country": "Philippines"
}
*/

// JSON Arryas
/*
"cities" : [
    {"city": "Tokyo", "country": "Japan"},
    {"city": "New York", "country": "U.S."},
    {"city": "Manila", "country": "Philippines"},
]
*/

let batchesArr = [
    {batchName: 'Batch 153'},
    {batchName: 'Batch 154'},
]

let stringifiedObject = JSON.stringify(batchesArr)
console.log(stringifiedObject)
console.log(typeof stringifiedObject)

let receivedData = JSON.parse(stringifiedObject)
console.log(receivedData)
console.log(typeof receivedData)